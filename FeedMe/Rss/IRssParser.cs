﻿using System;
namespace FeedMe.Rss
{
    public interface IRssParser
    {
        Feed GetFeed(Rss rss);
    }
}
