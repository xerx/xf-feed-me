﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace FeedMe.Rss
{
    [XmlRoot("rss")]
    public class Rss
    {
        [XmlElement("channel")]
        public Channel Channel { get; set; }
    }
    public class Channel : BaseItem
    {
        [XmlElement("item")]
        public List<Item> Items { get; set; }
        [XmlElement("image")]
        public Image Image { get; set; }
    }
    public class BaseItem
    {
        [XmlElement("title")]
        public string Title { get; set; }
        [XmlElement("description")]
        public string Description { get; set; }
        [XmlElement("link")]
        public string Link { get; set; }
    }
    public class Image
    {
        [XmlElement("url")]
        public string Url { get; set; }
    }
    public class Item : BaseItem
    {
        [XmlElement("pubDate")]
        public string PubDate { get; set; }
        [XmlElement("category")]
        public List<string> Categories { get; set; }
    }
}
