﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace FeedMe.Rss
{
    public class RssParser : IRssParser
    {
        public Feed GetFeed(Rss rss)
        {
            var feed = new Feed
            {
                Title = rss.Channel.Title,
                Description = rss.Channel.Description,
                Link = rss.Channel.Link,
                Image = rss.Channel.Image?.Url
            };
            if (string.IsNullOrEmpty(feed.Image))
            {
                var uri = new Uri(feed.Link);
                feed.Image = $"{uri.Scheme}://{uri.Host}/favicon.ico";
            }
            feed.Articles = rss.Channel.Items
                                       .Select(item => GetArticle(item))
                                       .OrderByDescending(article => article.Date)
                                       .ToList();
            return feed;
        }

        private Article GetArticle(Item item)
        {
            var article = new Article
            {
                Title = item.Title,
                Description = item.Description,
                Link = item.Link,
                Categories = item.Categories,
                Date = DateTime.Parse(item.PubDate).ToUniversalTime()
            };
            var imageMatch = Regex.Match(article.Description, "(?<=img src=\")[^\"]+(?=\")");
            if(imageMatch.Success)
            {
                article.Image = imageMatch.Value;
            }
            return article;
        }
    }
}
