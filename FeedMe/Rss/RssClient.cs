﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FeedMe.Rss
{
    public class RssClient : IRssClient
    {
        private readonly HttpClient httpClient;
        private readonly XmlSerializer serializer;
        private readonly IRssParser rssParser;

        public RssClient(IRssParser rssParser)
        {
            this.rssParser = rssParser;
            httpClient = new HttpClient();
            serializer = new XmlSerializer(typeof(Rss));
        }
        public async Task<Feed> FetchFeed(string url)
        {
            try
            {
                var response = await httpClient.GetStringAsync(url);
                var rss = (Rss)serializer.Deserialize(new StringReader(response));
                return rssParser.GetFeed(rss);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
