﻿using System;
using System.Threading.Tasks;

namespace FeedMe.Rss
{
    public interface IRssClient
    {
        Task<Feed> FetchFeed(string url);
    }
}
