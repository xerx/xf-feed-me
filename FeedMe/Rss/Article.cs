﻿using System;
using System.Collections.Generic;

namespace FeedMe.Rss
{
    public class Article
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string Image { get; set; }
        public DateTime Date { get; set; }
        public List<string> Categories { get; set; }
    }
}
