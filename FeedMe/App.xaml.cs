﻿using FeedMe.Rss;
using FeedMe.Views;
using Prism;
using Prism.Ioc;
using Xamarin.Forms;

[assembly: ExportFont("OpenSans-Light.ttf")]
[assembly: ExportFont("OpenSans-Bold.ttf")]
[assembly: ExportFont("materialdesignicons-webfont.ttf", Alias = "Material")]
namespace FeedMe
{
    [AutoRegisterForNavigation]
    public partial class App
    {
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer)
        {
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            await NavigationService.NavigateAsync($"{nameof(NavigationPage)}/{HomePage.ID}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterSingleton<IRssParser, RssParser>();
            containerRegistry.RegisterSingleton<IRssClient, RssClient>();
        }
    }
}

