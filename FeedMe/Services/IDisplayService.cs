﻿using System;
using Xamarin.Forms;

namespace FeedMe.Services
{
    public interface IDisplayService
    {
        Thickness SafeScreenAreaInsets { get; }
    }
}
