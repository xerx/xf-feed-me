﻿using System;
using System.Linq;
using System.Windows.Input;
using FeedMe.Rss;
using Sharpnado.MaterialFrame;
using Xamarin.Forms;

namespace FeedMe.Views
{
    public partial class ArticleView : MaterialFrame
    {
        private static readonly Color[] ChipColors = new Color[]
        {
            Color.FromHex("#264653"),
            Color.FromHex("#2a9d8f"),
            Color.FromHex("#e9c46a"),
            Color.FromHex("#f4a261"),
            Color.FromHex("#e76f51")
        };
        private Article article;
        public Article Article
        {
            get => article;
            set
            {
                article = value;
                ArticleChanged();
            }
        }
        public ICommand Command { get; set; }

        public ArticleView()
        {
            InitializeComponent();
        }
        private void ArticleChanged()
        {
            if (Article != null)
            {
                SetPopularCategory();
                titleLabel.Text = Article.Title;
                titleLabel.MaxLines = GetLinesForTitleLength(Article.Title.Length);
                dateLabel.Text = Article.Date.ToString("d");
                if (!string.IsNullOrEmpty(Article.Image))
                {
                    image.Source = Article.Image;
                }
                else
                {
                    image.IsVisible = false;
                }
            }
        }
        private void SetPopularCategory()
        {
            try
            {
                var category = Article.Categories.GroupBy(cat => cat)
                                                 .OrderByDescending(group => group.Count())
                                                 .First()
                                                 .First();
                categoryLabel.Text = category;
            }
            catch
            {
                categoryLabel.Text = "Uncategorised";
            }
            categoryFrame.BackgroundColor = GetRandomChipColor();
        }

        private void Tapped(object sender, EventArgs e)
        {
            if(Command?.CanExecute(Article) ?? false)
            {
                Command.Execute(Article);
            }
        }

        private static int GetLinesForTitleLength(int len) =>
            len > 80 ? 12 : len > 40 ? 6 : 2;

        private static Color GetRandomChipColor()
        {
            var i = new Random().Next(0, ChipColors.Length - 1);
            return ChipColors[i];
        }

    }
}
