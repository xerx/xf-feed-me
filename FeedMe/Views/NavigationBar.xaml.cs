﻿using System;
using System.Windows.Input;
using Sharpnado.MaterialFrame;
using Xamarin.Forms;

namespace FeedMe.Views
{
    public partial class NavigationBar : MaterialFrame
    {
        public const int BarHeight = 50;

        #region GenericCommand
        public static BindableProperty GenericCommandProperty =
                    BindableProperty.Create(nameof(GenericCommand),
                                            typeof(ICommand),
                                            typeof(NavigationBar));
        public ICommand GenericCommand
        {
            get => (ICommand)GetValue(GenericCommandProperty);
            set => SetValue(GenericCommandProperty, value);
        }
        #endregion
        #region GenericCommandParameter
        public static BindableProperty GenericCommandParameterProperty =
                    BindableProperty.Create(nameof(GenericCommandParameter),
                                            typeof(object),
                                            typeof(NavigationBar));
        public object GenericCommandParameter
        {
            get => GetValue(GenericCommandParameterProperty);
            set => SetValue(GenericCommandParameterProperty, value);
        }
        #endregion
        #region GenericButtonIcon
        public static BindableProperty GenericButtonIconProperty =
                    BindableProperty.Create(nameof(GenericButtonIcon),
                                            typeof(string),
                                            typeof(NavigationBar),
                                            propertyChanged: (b, o, n) => (b as NavigationBar).GenericButtonIconChanged());
        private void GenericButtonIconChanged()
        {
            genericButton.Text = GenericButtonIcon;
        }
        public string GenericButtonIcon
        {
            get => (string)GetValue(GenericButtonIconProperty);
            set => SetValue(GenericButtonIconProperty, value);
        }
        #endregion
        public NavigationBar()
        {
            InitializeComponent();
        }
        private void GenericButtonClicked(object sender, EventArgs e)
        {
            if(GenericCommand?.CanExecute(GenericCommandParameter) ?? false)
            {
                GenericCommand.Execute(GenericCommandParameter);
            }
        }
    }
}
