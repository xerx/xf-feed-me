﻿using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace FeedMe.Views
{
    public partial class LoadingOverlay : ContentView
    {
        public LoadingOverlay()
        {
            InitializeComponent();
            IsVisible = false;
        }
        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if(propertyName == IsVisibleProperty.PropertyName)
            {
                animation.IsPlaying = IsVisible;
            }
        }
    }
}
