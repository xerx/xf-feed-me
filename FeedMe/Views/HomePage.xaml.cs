﻿using Xamarin.Forms;

namespace FeedMe.Views
{
    public partial class HomePage : ContentPage
    {
        public const string ID = nameof(HomePage);
        public HomePage()
        {
            InitializeComponent();
        }
    }
}
