﻿using System;
using System.Collections.Generic;
using Sharpnado.MaterialFrame;
using Xamarin.Forms;

namespace FeedMe.Views
{
    public partial class MyPage : ContentPage
    {
        public MyPage()
        {
            InitializeComponent();
            Device.StartTimer(TimeSpan.FromSeconds(3), () =>
            {
                aa.Children.Add(new MaterialFrame
                {
                    MaterialTheme = MaterialFrame.Theme.AcrylicBlur,
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Center,
                    HeightRequest = 120,
                    WidthRequest = 120,
                    AndroidBlurRootElement = aa,
                    Content = new BoxView
                    {
                        BackgroundColor = Color.Red
                    }
                });
                return false;
            });
        }
    }
}
