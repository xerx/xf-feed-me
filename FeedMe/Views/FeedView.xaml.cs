﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace FeedMe.Views
{
    public partial class FeedView : ContentView
    {
        #region Text
        public static BindableProperty TextProperty =
                    BindableProperty.Create(nameof(Text),
                                            typeof(string),
                                            typeof(FeedView),
                                            propertyChanged: (b, o, n) => (b as FeedView).TextChanged());
        private void TextChanged()
        {
            label.Text = Text;
        }
        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }
        #endregion
        #region IsFocused
        public new static BindableProperty IsFocusedProperty =
                    BindableProperty.Create(nameof(IsFocused),
                                            typeof(bool),
                                            typeof(FeedView),
                                            propertyChanged: (b, o, n) => (b as FeedView).IsFocusedChanged());
        private void IsFocusedChanged()
        {
            if(IsFocused)
            {
                frame.ScaleTo(1);
            }
            else
            {
                frame.ScaleTo(.9);
            }
        }
        public new bool IsFocused
        {
            get => (bool)GetValue(IsFocusedProperty);
            set => SetValue(IsFocusedProperty, value);
        }
        #endregion
        #region Command
        public static BindableProperty CommandProperty =
                    BindableProperty.Create(nameof(Command),
                                            typeof(ICommand),
                                            typeof(FeedView));
        public ICommand Command
        {
            get => (ICommand)GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }
        #endregion
        #region CommandParameter
        public static BindableProperty CommandParameterProperty =
                    BindableProperty.Create(nameof(CommandParameter),
                                            typeof(object),
                                            typeof(FeedView));
        public object CommandParameter
        {
            get => GetValue(CommandParameterProperty);
            set => SetValue(CommandParameterProperty, value);
        }
        #endregion
        public FeedView()
        {
            InitializeComponent();
            frame.ScaleTo(.9);
        }

        private void Tapped(object sender, EventArgs e)
        {
            if(Command?.CanExecute(CommandParameter) ?? false)
            {
                Command.Execute(CommandParameter);
            }
        }
    }
}
