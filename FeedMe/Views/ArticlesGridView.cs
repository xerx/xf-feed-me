﻿
using System;
using System.Collections.Generic;
using FeedMe.ViewModels;
using Xamarin.Forms;

namespace FeedMe.Views
{
    public class ArticlesGridView : Grid
    {
        private const int RowHeight = 150;
        #region ItemsSource
        public static BindableProperty ItemsSourceProperty =
                    BindableProperty.Create(nameof(ItemsSource),
                                            typeof(IList<ArticleViewModel>),
                                            typeof(ArticlesGridView),
                                            propertyChanged: (b, o, n) => (b as ArticlesGridView).ItemsSourceChanged());
        private void ItemsSourceChanged()
        {
            Clear();
            ConstructGrid();
        }

        public IList<ArticleViewModel> ItemsSource
        {
            get => (IList<ArticleViewModel>)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }
        #endregion

        public Layout RootLayout { get; set; }

        public ArticlesGridView()
        {
            ColumnDefinitions = new ColumnDefinitionCollection
            {
                new ColumnDefinition(),
                new ColumnDefinition()
            };
            RowDefinitions = new RowDefinitionCollection();
        }
        private void ConstructGrid()
        {
            if(ItemsSource?.Count > 0)
            {
                int numColumn1Rows = 0;
                int numColumn2Rows = 0;
                for (int i = 0; i < ItemsSource.Count; i += 2)
                {
                    var (article1RowSpan, article2RowSpan) = TryGetRowSpanForArticles(i);

                    numColumn1Rows += article1RowSpan;
                    numColumn2Rows += article2RowSpan;
                    var maxRowSpan = Math.Max(numColumn1Rows, numColumn2Rows);
                    AddRowsRequired(maxRowSpan);
                    AddArticleView(i, 0, numColumn1Rows - article1RowSpan, article1RowSpan);
                    AddArticleView(i + 1, 1, numColumn2Rows - article2RowSpan, article2RowSpan);
                }
            }
        }
        private void AddArticleView(int articleIndex,
                                    int columnIndex,
                                    int rowIndex,
                                    int rowSpan)
        {
            try
            {
                var article1View = GetArticleView(articleIndex);
                Children.Add(article1View, columnIndex, rowIndex);
                SetRowSpan(article1View, rowSpan);
            }
            catch
            {
            }
        }
        private void AddRowsRequired(int maxRowSpan)
        {
            while (RowDefinitions.Count < maxRowSpan)
            {
                RowDefinitions.Add(new RowDefinition
                {
                    Height = RowHeight
                });
            }
        }

        private ArticleView GetArticleView(int index) =>
            new ArticleView
            {
                Article = ItemsSource[index].Article,
                Command = ItemsSource[index].Command,
                AndroidBlurRootElement = RootLayout
            };

        private (int, int) TryGetRowSpanForArticles(int index)
        {
            (int, int) rowSpans = (0, 0);
            try
            {
                var len = ItemsSource[index].Article.Title.Length;
                rowSpans.Item1 = GetRowSpanForTitleLength(len);
                len = ItemsSource[index + 1].Article.Title.Length;
                rowSpans.Item2 = GetRowSpanForTitleLength(len);
            }
            catch
            {
            }
            return rowSpans;
        }

        private static int GetRowSpanForTitleLength(int len) =>
            len > 80 ? 3 : len > 40 ? 2 : 1;

        private void Clear()
        {
            Children.Clear();
            RowDefinitions.Clear();
        }
    }
}

