﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace FeedMe.Views
{
    public partial class ArticlePage : ContentPage
    {
        public const string ID = nameof(ArticlePage);
        public ArticlePage()
        {
            InitializeComponent();
        }
    }
}
