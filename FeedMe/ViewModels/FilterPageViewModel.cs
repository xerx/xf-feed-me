﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using FeedMe.Services;
using Prism.Navigation;
using Xamarin.Forms;

namespace FeedMe.ViewModels
{
    public class FilterPageViewModel : ViewModelBase
    {
        private IEnumerable<string> _categories;
        public IEnumerable<string> Categories
        {
            get => _categories;
            set => SetProperty(ref _categories, value);
        }
        private string _selectedCategory;
        public string SelectedCategory
        {
            get => _selectedCategory;
            set
            {
                if(string.IsNullOrEmpty(value))
                {
                    return;
                }
                if (SetProperty(ref _selectedCategory, value))
                {
                    NavigateToBack();
                }
            }
        }
        public Thickness NavigationBarPadding { get; set; }
        public ICommand BackCommand { get; set; }

        public FilterPageViewModel(INavigationService navigationService,
                                    IDisplayService displayService) : base(navigationService)
        {
            NavigationBarPadding = new Thickness(6, displayService.SafeScreenAreaInsets.Top + 6, 6, 0);
            Title = "Filter categories";
            BackCommand = new Command(NavigateToBack);
        }
        public override void Initialize(INavigationParameters parameters)
        {
            base.Initialize(parameters);
            if(parameters.TryGetValue("categories", out IEnumerable<string> categories))
            {
                Categories = categories.Distinct().OrderBy(str => str);
            }
        }
        private async void NavigateToBack()
        {
            IsBusy = true;
            var selectionCopy = SelectedCategory;
            SelectedCategory = null;
            await NavigationService.GoBackAsync(new NavigationParameters
            {
                { "category", selectionCopy }
            },
            true);
            IsBusy = false;
        }
    }
}

