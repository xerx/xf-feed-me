﻿using System;
using System.Windows.Input;
using FeedMe.Rss;
using FeedMe.Services;
using Prism.Navigation;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace FeedMe.ViewModels
{
    public class ArticlePageViewModel : ViewModelBase
    {
        private WebViewSource _webViewSource;
        public WebViewSource WebViewSource
        {
            get => _webViewSource;
            set => SetProperty(ref _webViewSource, value);
        }
        public Thickness NavigationBarPadding { get; set; }
        public Thickness WebViewPadding { get; set; }
        public ICommand BackCommand { get; set; }
        public ICommand ShareCommand { get; set; }
        public Article Article { get; set; }

        public ArticlePageViewModel(INavigationService navigationService,
                                    IDisplayService displayService) : base(navigationService)
        {
            NavigationBarPadding = new Thickness(6, displayService.SafeScreenAreaInsets.Top + 6, 6, 0);
            WebViewPadding = new Thickness(6, 6, 6, displayService.SafeScreenAreaInsets.Bottom);
            BackCommand = new Command(NavigateToBack);
            ShareCommand = new Command(ShareArticle);
        }

        public override void Initialize(INavigationParameters parameters)
        {
            base.Initialize(parameters);
            if (parameters.TryGetValue("article", out Article article))
            {
                Article = article;
                WebViewSource = article.Link;
                Title = article.Title;
            }
        }

        private void ShareArticle()
        {
            Share.RequestAsync(new ShareTextRequest
            {
                Title = "Share article",
                Uri = Article.Link
            });
        }
        private async void NavigateToBack()
        {
            IsBusy = true;
            await NavigationService.GoBackAsync();
            IsBusy = false;
        }
        public override void Destroy()
        {
            base.Destroy();
            WebViewSource = null;
            Article = null;
        }
    }
}

