﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using FeedMe.Helpers;
using FeedMe.Rss;
using FeedMe.Services;
using FeedMe.Views;
using Prism.Navigation;
using Xamarin.Forms;

namespace FeedMe.ViewModels
{
    public class FeedPageViewModel : ViewModelBase
    {
        private List<ArticleViewModel> _filteredArticles;
        public List<ArticleViewModel> FilteredArticles
        {
            get => _filteredArticles;
            set => SetProperty(ref _filteredArticles, value);
        }
        private ICommand _filterCommand;
        public ICommand FilterCommand
        {
            get => _filterCommand;
            set => SetProperty(ref _filterCommand, value);
        }
        private string _filterButtonIcon;
        public string FilterButtonIcon
        {
            get => _filterButtonIcon;
            set => SetProperty(ref _filterButtonIcon, value);
        }
        public Thickness ScrollViewPadding { get; set; }
        public Thickness NavigationBarPadding { get; set; }
        public ICommand BackCommand { get; set; }
        public ICommand ArticleSelectedCommand { get; set; }

        private IEnumerable<string> articlesCategories;
        private List<ArticleViewModel> allArticles;

        public FeedPageViewModel(INavigationService navigationService,
                                 IDisplayService displayService) : base(navigationService)
        {
            var topInset = displayService.SafeScreenAreaInsets.Top;
            ScrollViewPadding = new Thickness(0, topInset + NavigationBar.BarHeight + 18, 0, 0);
            NavigationBarPadding = new Thickness(6, topInset + 6, 6, 0);
            BackCommand = new Command(NavigateToBack);
            ArticleSelectedCommand = new Command<Article>(ArticleSelected);
            FilterButtonIcon = IconFont.FilterVariant;
        }

        public override void Initialize(INavigationParameters parameters)
        {
            base.Initialize(parameters);
            if(parameters.TryGetValue("feed", out Feed feed))
            {
                allArticles = feed.Articles.Select(article =>
                    new ArticleViewModel(article, ArticleSelectedCommand)).ToList();
                FilteredArticles = allArticles;
                articlesCategories = FilteredArticles.SelectMany(vm => vm.Article.Categories);
                FilterCommand = new Command(FilterArticles, _ => articlesCategories.Count() > 0);
                Title = feed.Title;
            }
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.TryGetValue("category", out string categoryFilter))
            {
                ApplyFilter(categoryFilter);
            }
        }

        private async void ArticleSelected(Article article)
        {
            IsBusy = true;
            await NavigationService.NavigateAsync(ArticlePage.ID, new NavigationParameters
            {
                { "article", article }
            });
            IsBusy = false;
        }
        private async void FilterArticles(object obj)
        {
            if (FilterButtonIcon == IconFont.FilterVariant)
            {
                await NavigateToFilterPage();
            }
            else
            {
                ClearFilters();
            }
        }
        private void ApplyFilter(string categoryFilter)
        {
            FilterButtonIcon = IconFont.FilterVariantRemove;
            FilteredArticles = allArticles.Where(vm => vm.Article.Categories.Contains(categoryFilter))
                                          .ToList();
        }

        private void ClearFilters()
        {
            FilterButtonIcon = IconFont.FilterVariant;
            FilteredArticles = allArticles;
        }

        private async Task NavigateToFilterPage()
        {
            IsBusy = true;
            await NavigationService.NavigateAsync(FilterPage.ID, new NavigationParameters
            {
                { "categories", articlesCategories }
            },
            true);
            IsBusy = false;
        }

        private async void NavigateToBack()
        {
            IsBusy = true;
            await NavigationService.GoBackAsync();
            IsBusy = false;
        }
        public override void Destroy()
        {
            base.Destroy();
            FilteredArticles = null;
            allArticles = null;
        }
    }
}

