﻿using System;
using System.Windows.Input;
using FeedMe.Rss;
using Prism.Mvvm;

namespace FeedMe.ViewModels
{
    public class ArticleViewModel : BindableBase
    {
        public Article Article { get; }
        public ICommand Command { get; }

        public ArticleViewModel(Article article, ICommand command)
        {
            Article = article;
            Command = command;
        }
    }
}
