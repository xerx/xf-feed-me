﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using FeedMe.Rss;
using FeedMe.Views;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Essentials;

namespace FeedMe.ViewModels
{
    public class HomePageViewModel : ViewModelBase
    {
        private List<FeedViewModel> _feeds;
        public List<FeedViewModel> Feeds
        {
            get => _feeds;
            set => SetProperty(ref _feeds, value);
        }
        private string _currentLogo;
        public string CurrentLogo
        {
            get => _currentLogo;
            set => SetProperty(ref _currentLogo, value);
        }
        private int _currentFeedPosition;
        public int CurrentFeedPosition
        {
            get => _currentFeedPosition;
            set
            {
                var previousPosition = _currentFeedPosition;
                if(SetProperty(ref _currentFeedPosition, value))
                {
                    FocusedFeedChanged(previousPosition);
                }
            }
        }
        private bool _noNetwork;
        public bool NoNetwork
        {
            get => _noNetwork;
            set => SetProperty(ref _noNetwork, value);
        }
        public ICommand FeedSelectedCommand { get; set; }

        private readonly string[] feedUrls = new string[]
        {
            "https://blogs.microsoft.com/feed/",
            "https://dev.to/feed",
            "https://feeds.bbci.co.uk/news/business/rss.xml",
            "https://montemagno.com/rss/"
        };

        private readonly IRssClient rssClient;

        public HomePageViewModel(INavigationService navigationService,
                                 IRssClient rssClient) : base(navigationService)
        {
            this.rssClient = rssClient;
            FeedSelectedCommand = new DelegateCommand<FeedViewModel>(FeedSelected);
        }

        public override async void Initialize(INavigationParameters parameters)
        {
            base.Initialize(parameters);

            if(Connectivity.NetworkAccess == NetworkAccess.Internet)
            {
                await FetchFeeds();
            }
            else
            {
                NoNetwork = true;
            }
        }

        private async Task FetchFeeds()
        {
            IsBusy = true;
            var feedsList = new List<Feed>();
            foreach (var url in feedUrls)
            {
                feedsList.Add(await rssClient.FetchFeed(url));
            }
            Feeds = feedsList.Select(feed => new FeedViewModel(feed, FeedSelectedCommand)).ToList();
            FocusedFeedChanged(-1);
            IsBusy = false;
        }

        private void FocusedFeedChanged(int previousPosition)
        {
            CurrentLogo = Feeds[CurrentFeedPosition].Image;
            Feeds[CurrentFeedPosition].IsFocused = true;
            if (previousPosition > -1)
            {
                Feeds[previousPosition].IsFocused = false;
            }
        }

        private async void FeedSelected(FeedViewModel feedViewModel)
        {
            IsBusy = true;
            await NavigationService.NavigateAsync(FeedPage.ID, new NavigationParameters
            {
                { "feed", feedViewModel.Feed }
            });
            IsBusy = false;
        }
    }
}
