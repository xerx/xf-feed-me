﻿using System.Windows.Input;
using FeedMe.Rss;
using Prism.Mvvm;

namespace FeedMe.ViewModels
{
    public class FeedViewModel : BindableBase
    {
        private bool _isFocused;
        public bool IsFocused
        {
            get => _isFocused;
            set => SetProperty(ref _isFocused, value);
        }
        public string Title => Feed.Title;
        public string Image => Feed.Image;
        public int NumArticles => Feed.Articles.Count;

        public Feed Feed { get; set; }
        public ICommand Command { get; set; }

        public FeedViewModel(Feed feed, ICommand command = null)
        {
            Feed = feed;
            Command = command;
        }
    }
}
