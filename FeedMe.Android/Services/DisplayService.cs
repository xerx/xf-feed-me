﻿using System;
using FeedMe.Services;
using Xamarin.Forms;

namespace FeedMe.Droid.Services
{
    public class DisplayService : IDisplayService
    {
        public Thickness SafeScreenAreaInsets => new Thickness();
    }
}
