﻿using FeedMe.Services;
using UIKit;
using Xamarin.Forms;

namespace FeedMe.iOS.Services
{
    public class DisplayService : IDisplayService
    {
        public Thickness SafeScreenAreaInsets
        {
            get
            {
                try
                {
                    if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
                    {
                        UIEdgeInsets insets = UIApplication.SharedApplication.Windows[0].SafeAreaInsets;
                        return new Thickness(insets.Left, insets.Top, insets.Right, insets.Bottom);
                    }
                    return new Thickness(0, 20, 0, 0);
                }
                catch
                {
                    return new Thickness();
                }
            }
        }
    }
}
